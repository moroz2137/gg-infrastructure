```shell
docker-compose -f /usr/local/gg/docker-compose.yml stop web
docker-compose -f /usr/local/gg/docker-compose.yml exec pg dropdb gg_production
docker-compose -f /usr/local/gg/docker-compose.yml exec pg createdb gg_production
docker-compose -f /usr/local/gg/docker-compose.yml exec pg pg_restore -U app -d gg_production /var/db_dumps/20220704.dump
```

```shell
tar xf ~/assets-new.tar.gz -C /usr/local/gg/uploads/images/
```
