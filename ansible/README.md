## Prepare files

 - vault.key

## Encrypt

ansible-vault decrypt payment.json

## Execute

ansible-playbook -v site.yml --tags xxxxx

### Run Only to Production 
ansible-playbook -v site.yml --tags secrets --limit production

## Modify Template
sorry, template file is created as sametime with initialization