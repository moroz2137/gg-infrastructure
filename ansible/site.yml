---
- name: Install docker
  hosts: all
  remote_user: "{{ user_name }}"
  become: true
  tags: docker

  roles:
    - geerlingguy.docker

- name: Install service dependencies
  hosts: all
  remote_user: "{{ user_name }}"
  become: true
  tags: deps

  tasks:
    - name: Ensure awscli is installed
      yum:
        name: awscli
        update_cache: true
        state: present

    - name: Ensure docker for python is installed
      pip:
        name:
          - docker
          - docker-compose

    - name: Ensure deployer user can run docker commands
      user:
        name: "{{ deploy_user }}"
        append: yes
        groups:
          - docker

- name: Set up Docker services
  hosts: all
  remote_user: "{{ user_name }}"
  become: true
  tags: services

  tasks:
    - name: Ensure folders exist
      file:
        path: "{{ item }}"
        owner: "{{ deploy_user }}"
        group: docker
        mode: '0755'
        state: directory
      with_items:
        - "{{ root_folder }}"
        - "{{ nginx_folder }}"
        - "{{ log_folder }}"
        - "{{ uploads_folder }}"
        - "{{ db_dump_folder }}"

    - name: Ensure a docker-compose.yml file exists
      notify:
        - Restart docker compose services
      template:
        src: docker-compose.yml.j2
        dest: "{{ docker_compose_file_path }}"
        owner: "{{ deploy_user }}"
        group: docker
        mode: '0640'

    - name: Check if a certificate exists for API domain
      stat:
        path: "{{ certbot_certs_path }}/live/{{ api_domain }}/privkey.pem"
      register: certificate_file

    - name: Ensure a custom nginx.conf exists
      notify:
        - Restart docker compose services
      vars:
        certificate_exists: "{{ certificate_file.stat.exists }}"
      template:
        src: nginx.conf.docker.j2
        dest: "{{ nginx_conf_path }}"
        owner: "{{ deploy_user }}"
        group: docker
        mode: '0640'

    - name: Log in to ECR
      ignore_errors: true
      include_tasks: tasks/docker-login.yml

  handlers:
    - name: Restart docker compose services
      docker_compose:
        project_src: "{{ root_folder }}"
        restarted: yes
      ignore_errors: true

- name: Request a Let's Encrypt Certificate
  hosts: all
  remote_user: "{{ user_name }}"
  become: true
  tags: ssl

  tasks:
    - name: Check if a certificate exists for API domain
      stat:
        path: "{{ certbot_certs_path }}/{{ api_domain }}/privkey.pem"
      register: certificate_file

    - name: Ensure the folder for ACME challenge exists
      when: not certificate_file.stat.exists
      file:
        path: "{{ acme_challenge_path }}/{{ api_domain }}"
        owner: "{{ deploy_user }}"
        group: docker
        mode: '0775'
        state: directory

    - name: Request a certificate using dockerized Certbot
      when: not certificate_file.stat.exists
      register: certificate_created
      shell:
        cmd: |
          /usr/local/bin/docker-compose -f {{ docker_compose_file_path }} \
          exec -T certbot certbot certonly --webroot \
          -w "/var/www/certbot/{{ api_domain }}" \
          -d "{{ api_domain }}" --email {{ certbot_admin_email }} \
          --rsa-key-size 4096 --agree-tos --noninteractive

    - name: Recreate nginx.conf
      when: certificate_created is changed
      vars:
        certificate_exists: true
      template:
        src: nginx.conf.docker.j2
        dest: "{{ nginx_conf_path }}"
        owner: "{{ deploy_user }}"
        group: docker
        mode: '0640'

    - name: Restart docker compose services
      when: certificate_created is changed
      docker_compose:
        project_src: "{{ root_folder }}"
        restarted: yes
      ignore_errors: true

- name: Install CodeDeploy agent
  hosts: all
  remote_user: "{{ user_name }}"
  become: true
  tags: codedeploy

  roles:
    - diodonfrost.amazon_codedeploy
