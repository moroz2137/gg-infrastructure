variable "base_name" {}

variable "git_repo_name" {}

variable "deployment_role" {
  default = null
}

variable "codedeploy_app_name" {
  type    = string
  default = null
}

variable "codebuild_image" {
  type    = string
  default = "aws/codebuild/standard:5.0"
}

variable "dockerhub_credential" {
  type    = string
  default = null
}

variable "deployment_group_name" {
  type    = string
  default = null
}

variable "git_branch" {
  type    = string
  default = "master"
}

variable "aws_region" {
  type = string
}

variable "additional_build_env_vars" {
  type    = map(any)
  default = {}
}

variable "auto_expire_artifacts" {
  type    = bool
  default = false
}

variable "auto_expire_days" {
  type    = number
  default = 3
}

variable "schedule_runs" {
  type    = bool
  default = false
}

variable "schedule_expression" {
  type    = string
  default = "cron(55 21 * * ? *)"
}

variable "is_arm" {
  type    = bool
  default = false
}

variable "enable_deploy" {
  type    = bool
  default = true
}
