locals {
  pipeline_name          = "${var.base_name}-codepipeline"
  app_name               = "${var.base_name}-app"
  artifact_bucket_name   = "${local.pipeline_name}-artifacts"
  codepipeline_role_name = "${local.pipeline_name}-service-role"
  codebuild_project_name = "${var.base_name}-build"
  codebuild_role_name    = "${local.codebuild_project_name}-service-role"
  deployment_group_name  = var.deployment_group_name
}

resource "aws_s3_bucket" "pipeline_artifacts" {
  bucket        = local.artifact_bucket_name
  force_destroy = true
}

resource "aws_s3_bucket_lifecycle_configuration" "pipeline_artifacts_lifecycle" {
  bucket = aws_s3_bucket.pipeline_artifacts.id

  rule {
    id     = "autoexpire"
    status = var.auto_expire_artifacts ? "Enabled" : "Disabled"

    expiration {
      days = var.auto_expire_days
    }
  }
}

resource "aws_codebuild_project" "codebuild_project" {
  name          = local.codebuild_project_name
  service_role  = aws_iam_role.codebuild_role.arn
  badge_enabled = false

  artifacts {
    type = "CODEPIPELINE"
  }

  cache {
    type  = var.is_arm ? "NO_CACHE" : "LOCAL"
    modes = var.is_arm ? null : ["LOCAL_CUSTOM_CACHE", "LOCAL_SOURCE_CACHE"]
  }

  environment {
    compute_type                = var.is_arm ? "BUILD_GENERAL1_LARGE" : "BUILD_GENERAL1_SMALL"
    image                       = var.codebuild_image
    privileged_mode             = false
    type                        = var.is_arm ? "ARM_CONTAINER" : "LINUX_CONTAINER"
    image_pull_credentials_type = var.dockerhub_credential == null ? "CODEBUILD" : "SERVICE_ROLE"


    dynamic "registry_credential" {
      for_each = var.dockerhub_credential == null ? [] : [1]

      content {
        credential          = var.dockerhub_credential
        credential_provider = "SECRETS_MANAGER"
      }
    }

    dynamic "environment_variable" {
      for_each = var.additional_build_env_vars
      content {
        name  = environment_variable.key
        value = environment_variable.value
      }
    }
  }

  logs_config {
    cloudwatch_logs {
      status = "ENABLED"
    }

    s3_logs {
      encryption_disabled = false
      status              = "DISABLED"
    }
  }

  source {
    type            = "CODEPIPELINE"
    git_clone_depth = 0
  }
}

resource "aws_codepipeline" "cd_pipeline" {
  name     = local.pipeline_name
  role_arn = aws_iam_role.codepipeline_role.arn

  artifact_store {
    type     = "S3"
    location = aws_s3_bucket.pipeline_artifacts.bucket
  }


  stage {
    name = "Source"

    action {
      category        = "Source"
      owner           = "AWS"
      provider        = "CodeCommit"
      input_artifacts = []
      name            = "Source"
      output_artifacts = [
        "SourceArtifact"
      ]
      run_order = 1
      version   = "1"

      configuration = {
        "RepositoryName" = var.git_repo_name
        "BranchName"     = var.git_branch

        # If the pipeline is supposed to run on a schedule, we won't trigger
        # a deployment on each revision.
        "PollForSourceChanges" = !var.schedule_runs
      }
    }
  }

  stage {
    name = "Build"

    action {
      category = "Build"
      owner    = "AWS"
      provider = "CodeBuild"
      configuration = {
        "ProjectName" = aws_codebuild_project.codebuild_project.name
      }
      input_artifacts = [
        "SourceArtifact"
      ]
      output_artifacts = [
        "BuildArtifact"
      ]
      name      = "Build"
      run_order = 1
      version   = "1"
    }
  }

  dynamic "stage" {
    for_each = var.enable_deploy ? [true] : []
    content {

      name = "Deploy"

      action {
        name     = "Deploy"
        category = "Deploy"
        owner    = "AWS"
        provider = "CodeDeploy"

        input_artifacts = ["BuildArtifact"]
        version         = "1"

        configuration = {
          "ApplicationName"     = var.codedeploy_app_name
          "DeploymentGroupName" = var.deployment_group_name
        }
      }
    }
  }
}



