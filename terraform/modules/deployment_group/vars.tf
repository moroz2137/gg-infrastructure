variable "app_name" {
  type = string
}

variable "deployment_group_name" {
  type = string
}
