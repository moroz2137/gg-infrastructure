resource "aws_subnet" "public" {
  provider          = aws.region
  vpc_id            = aws_vpc._.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = var.aws_availability_zones[1]

  tags = {
    Name = "${var.namespace}-public-subnet"
  }
}

output "public_subnets" {
  value = [
    aws_subnet.public
  ]
}
