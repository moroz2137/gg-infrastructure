terraform {
  required_providers {
    aws = {
      source                = "hashicorp/aws"
      configuration_aliases = [aws.region]
    }
  }
}

resource "aws_instance" "_" {
  provider               = aws.region
  ami                    = var.server_ami
  instance_type          = var.instance_type
  key_name               = var.key_name
  vpc_security_group_ids = [aws_security_group.ec2.id]
  subnet_id              = aws_subnet.public.id
  iam_instance_profile   = aws_iam_instance_profile._.name

  lifecycle {
    ignore_changes = [ami]
  }

  tags = {
    Name                = var.namespace
    DeploymentGroupName = var.namespace
  }

  root_block_device {
    volume_size = 65
  }
}

resource "aws_eip" "_" {
  provider = aws.region
  instance = aws_instance._.id
  vpc      = true
}
