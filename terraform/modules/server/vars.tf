variable "env" {
  type = string
}

variable "namespace" {
  type = string
}

variable "server_ami" {
  type = string
}

variable "aws_availability_zones" {
  type = list(string)
}

variable "instance_type" {
  type    = string
  default = "t4g.micro"
}

variable "key_name" {
  type = string
}

variable "permitted_buckets" {
  type    = list(string)
  default = []
}
