terraform {
  required_providers {
    aws = {
      source                = "hashicorp/aws"
      configuration_aliases = [aws.region]
    }
  }
}

locals {
  deployer_role_name    = "${var.project_name}-git-deployer"
  developer_policy_name = "${var.project_name}-developer-policy"
}

resource "aws_codecommit_repository" "app_repo" {
  provider        = aws.region
  repository_name = "${var.project_name}-backend"
}

locals {
  production_repos = [
    aws_codecommit_repository.app_repo
  ]
}
