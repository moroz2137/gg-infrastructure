variable "base_name" {}

variable "git_repo_name" {}

variable "deployment_role" {
  default = null
}

variable "codedeploy_app_name" {
  type    = string
  default = null
}

variable "codebuild_image" {
  type    = string
  default = "aws/codebuild/standard:6.0"
}

variable "deployment_group_name" {
  type    = string
  default = null
}

variable "git_branch" {
  type    = string
  default = "master"
}

variable "aws_region" {
  type = string
}

variable "additional_build_env_vars" {
  type    = map(any)
  default = {}
}

variable "enable_deploy" {
  type    = bool
  default = true
}

variable "buildspec" {
  type    = string
  default = "buildspec.yml"
}
