variable "env" {
  type = string
}

variable "environments" {
  type    = list(string)
  default = ["production"]
}

variable "project_name" {
  type = string
}

variable "aws_region" {
  type = string
}

variable "aws_availability_zones" {
  type = list(string)
}

variable "aws_account_id" {
  type = string
}

variable "deployment_branch" {
  type    = string
  default = "development"
}

variable "db_username" {
  type = string
}

variable "db_password" {
  type = string
}

variable "dockerhub_username" {
  type = string
}

variable "dockerhub_password" {
  type = string
}


variable "deployer_key" {
  type = string
}

variable "azure_region" {
  type    = string
  default = "Germany West Central"
}

variable "on_demand_pipelines" {
  type    = list(string)
  default = []
}

variable "docker_repos" {
  type    = list(string)
  default = []
}
