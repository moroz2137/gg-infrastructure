{
  "Version": "2012-10-17",
    "Statement": [
    %{if length(protected_repos) > 0}
    {
      "Effect": "Deny",
      "Action": [
        "codecommit:GitPush",
        "codecommit:DeleteBranch",
        "codecommit:PutFile",
        "codecommit:MergeBranchesByFastForward",
        "codecommit:MergeBranchesBySquash",
        "codecommit:MergeBranchesByThreeWay",
        "codecommit:MergePullRequestByFastForward",
        "codecommit:MergePullRequestBySquash",
        "codecommit:MergePullRequestByThreeWay"
      ],
      "Resource": ${jsonencode(protected_repos)},
      "Condition": {
        "StringEqualsIfExists": {
          "codecommit:References": ${jsonencode(branches)}
        },
        "Null": {
          "codecommit:References": false
        }
      }
    },
    %{endif}
    {
      "Effect": "Allow",
      "Action": [
        "codecommit:Git*",
        "codecommit:Put*",
        "codecommit:Merge*",
        "codecommit:CreatePullRequest",
        "codecommit:UpdatePullRequest*"
      ],
      "Resource": ["*"]
    }%{ if length(lax_repos) > 0 },
    {
      "Effect": "Allow",
      "Action": [
        "codecommit:*"
      ],
      "Resource": ${jsonencode(lax_repos)}
    }%{endif}
  ]
}
