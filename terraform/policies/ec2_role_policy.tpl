{
  "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": [
          "s3:*"
        ],
        "Resource": "arn:aws:s3:::jinmen-staging-backups/*"
      },
      {
        "Effect": "Allow",
        "Action": [
          "s3:Get*",
          "s3:List*",
          "ecr:GetAuthorizationToken",
          "ecr:BatchGetImage",
          "ecr:GetDownloadUrlForLayer"
        ],
        "Resource": ["*"]
      }%{if length(permitted_buckets) > 0},
        {
          "Effect": "Allow",
          "Action": [
            "s3:Create*",
            "s3:Get*",
            "s3:List*",
            "s3:Put*",
            "s3:PutBucketTagging"
          ],
          "Resource": ${jsonencode(permitted_buckets)}
        }
      %{endif}
    ]
}
