resource "aws_ecr_repository" "ecr" {
  count    = length(var.docker_repos)
  provider = aws.tokyo

  name = var.docker_repos[count.index]

  image_scanning_configuration {
    scan_on_push = true
  }
}

resource "aws_ecr_repository_policy" "ecr_policy" {
  count    = length(var.docker_repos)
  provider = aws.tokyo

  repository = aws_ecr_repository.ecr[count.index].name

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Principal = {
          Service = "codebuild.amazonaws.com"
        }
        Action = [
          "ecr:GetDownloadUrlForLayer",
          "ecr:BatchGetImage",
          "ecr:BatchCheckLayerAvailability"
        ]
      }
    ]
  })
}
