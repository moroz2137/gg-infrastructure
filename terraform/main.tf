resource "aws_key_pair" "deployer" {
  key_name   = "karol"
  public_key = var.deployer_key
}

resource "aws_key_pair" "deployer_tokyo" {
  provider   = aws.tokyo
  key_name   = "karol"
  public_key = var.deployer_key
}

locals {
  namespace         = "${var.project_name}-${var.env}"
  protected_buckets = []
}

module "server_docker" {
  providers = {
    aws.region = aws.tokyo
  }
  source        = "./modules/server"
  namespace     = "${var.project_name}-production"
  env           = "production"
  key_name      = aws_key_pair.deployer.key_name
  instance_type = "t2.small"
  server_ami    = data.aws_ami.centos.id
  aws_availability_zones = [
    "ap-northeast-1a",
    "ap-northeast-1c"
  ]
}

module "codecommit" {
  source = "./modules/codecommit"

  providers = {
    aws.region = aws.tokyo
  }
  project_name = var.project_name
  aws_region   = var.aws_region
}

resource "aws_codedeploy_app" "docker" {
  provider         = aws.tokyo
  compute_platform = "Server"
  name             = "${var.project_name}-docker"
}

module "deployment_group" {
  providers = {
    aws.region = aws.tokyo
  }

  source                = "./modules/deployment_group"
  count                 = length(var.environments)
  deployment_group_name = "${var.project_name}-${var.environments[count.index]}"
  app_name              = aws_codedeploy_app.docker.name
}

resource "aws_secretsmanager_secret" "docker" {
  name = "dockerhub"
}

resource "aws_secretsmanager_secret_version" "docker" {
  secret_id = aws_secretsmanager_secret.docker.id
  secret_string = jsonencode({
    username = var.dockerhub_username
    password = var.dockerhub_password
  })
}

module "docker_pipeline" {
  source    = "./modules/docker-pipeline"
  base_name = "${var.project_name}-docker-production"

  git_repo_name         = module.codecommit.node_repo.repository_name
  git_branch            = "production"
  deployment_group_name = "${var.project_name}-production"
  aws_region            = "ap-northeast-1"
  deployment_role       = module.deployment_group[0].deployment_role
  codebuild_image       = "aws/codebuild/standard:6.0"
  codedeploy_app_name   = aws_codedeploy_app.docker.name
  buildspec             = "buildspec-docker.yml"

  providers = {
    aws.region = aws.tokyo
  }

  additional_build_env_vars = {
    REPOSITORY_URI = aws_ecr_repository.ecr[0].repository_url
    AWS_ACCOUNT_ID = var.aws_account_id
  }
}

module "nextjs_pipeline" {
  source    = "./modules/docker-pipeline"
  base_name = "${var.project_name}-nextjs-production"

  git_repo_name         = "kcjh"
  git_branch            = "main"
  deployment_group_name = "${var.project_name}-production"
  aws_region            = "ap-northeast-1"
  deployment_role       = module.deployment_group[0].deployment_role
  codebuild_image       = "aws/codebuild/standard:6.0"
  codedeploy_app_name   = aws_codedeploy_app.docker.name

  providers = {
    aws.region = aws.tokyo
  }

  additional_build_env_vars = {
    REPOSITORY_URI          = aws_ecr_repository.ecr[1].repository_url
    AWS_ACCOUNT_ID          = var.aws_account_id
    NEXT_PUBLIC_CMS_API_URL = "https://kinmen.thebarkingdog.tw/api"
  }
}
