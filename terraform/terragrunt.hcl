locals {
  env         = get_env("ENV", "staging")
  base_vars   = yamldecode(file("./vars/${local.env}.yml"))
  env_secrets = yamldecode(file("./vars/${local.env}.secrets.yml"))
  secrets     = merge(local.base_vars, local.env_secrets)
}

remote_state {
  backend = "s3"
  config = {
    encrypt        = true
    bucket         = "${get_env("TG_BUCKET_PREFIX", "")}terraform-state-${local.secrets.project_name}-${local.env}-${local.secrets.aws_region}"
    key            = "${path_relative_to_include()}/terraform.tfstate"
    profile        = local.secrets.aws_profile
    region         = local.secrets.aws_region
    dynamodb_table = "terraform-locks"
  }
  generate = {
    path      = "backend.tf"
    if_exists = "overwrite_terragrunt"
  }
}

generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
provider "aws" {
  region = "${local.secrets.aws_region}"
  profile = "${local.secrets.aws_profile}"
}

provider "aws" {
  region = "ap-northeast-1"
  alias = "tokyo"
  profile = "${local.secrets.aws_profile}"
}
EOF
}

inputs = merge(
  local.secrets,
  {
    env = local.env
  }
)

